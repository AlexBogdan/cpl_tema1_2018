package cool.compiler;

import cool.parser.CoolParser;
import cool.parser.CoolParserBaseVisitor;
import java.lang.Void;

public class ASTVisitor<Void> extends CoolParserBaseVisitor<Void> {
	
	private int tabs = 0;
	
	private String printTabs() {
		String result = "";
		for (int i = 0; i < tabs; i++) {
			result += "  ";
		}
		
		return result;
	}
	
	@Override
	public Void visitProgram(CoolParser.ProgramContext ctx) {
		System.out.println(printTabs() + "program");
		tabs++;
//		Vizitam fiecare clasa din program
		for (var child : ctx.children) {
			visit(child);
		}
		tabs--;
		
		return null;
	}
	
//	Am gasit o clasa, vizitam componentele ei
	public Void visitClassDeclaration(CoolParser.ClassDeclarationContext ctx) {
		System.out.println(printTabs() + "class");
		tabs++;
		visit(ctx.className);
		if (ctx.classInherited != null) {
			visit(ctx.classInherited);
		}
		visit(ctx.classBody());
		tabs--;
		
		return null;
	}
	
//	Am gasit un atribut al unei clase, vizitam componentele sale
	public Void visitAttribute(CoolParser.AttributeContext ctx) {
		System.out.println(printTabs() + "attribute");
		tabs++;
		visit(ctx.attrName);
		visit(ctx.attrType);
		if (ctx.attrInitValue != null) {
			visit(ctx.attrInitValue);
		}
		tabs--;
		
		return null;
	}

//
//	Vizitam o metoda
//
	
//	Am gasit o metoda a unei clase, vizitam componentele sale
	public Void visitMethod(CoolParser.MethodContext ctx) {
		System.out.println(printTabs() + "method");
		tabs++;
		visit(ctx.methodName);
		visit(ctx.methodHeader());
		visit(ctx.methodType);
		visit(ctx.methodBody());
		tabs--;
		
		return null;
	}
	
	public Void visitFormal(CoolParser.FormalContext ctx) {
		System.out.println(printTabs() + "formal");
		tabs++;
		visit(ctx.formalName);
		visit(ctx.formalType);
		tabs--;
		
		return null;
	}

//
//	Vizitam expr-uri
//
	
	public Void visitAssignment(CoolParser.AssignmentContext ctx) {
		System.out.println(printTabs() + "<-");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitImplicitDispatch(CoolParser.ImplicitDispatchContext ctx) {
		System.out.println(printTabs() + "implicit dispatch");
		tabs++;
		visit(ctx.functionCall());
		tabs--;
		
		return null;
	}
	
	public Void visitDispatch(CoolParser.DispatchContext ctx) {
		System.out.println(printTabs() + ".");
		tabs++;
		visit(ctx.expr());
		if (ctx.cast() != null) {
			visit(ctx.cast());
		}
		visit(ctx.functionCall());
		tabs--;
		
		return null;
	}
	
	public Void visitCast(CoolParser.CastContext ctx) {
		visit(ctx.type());

		return null;
	}
	
	public Void visitIf(CoolParser.IfContext ctx) {
		System.out.println(printTabs() + "if");
		tabs++;
		visit(ctx.cond);
		visit(ctx.body1);
		visit(ctx.body2);
		tabs--;
		
		return null;
	}
	
	public Void visitWhile(CoolParser.WhileContext ctx) {
		System.out.println(printTabs() + "while");
		tabs++;
		visit(ctx.cond);
		visit(ctx.body);
		tabs--;
		
		return null;
	}
	
	public Void visitLet(CoolParser.LetContext ctx) {
		System.out.println(printTabs() + "let");
		tabs++;
		visit(ctx.vars());
		visit(ctx.body);
		tabs--;
		
		return null;
	}
	
	public Void visitLocal(CoolParser.LocalContext ctx) {
		System.out.println(printTabs() + "local");
		tabs++;
		visit(ctx.varName);
		visit(ctx.varType);
		if (ctx.varInitValue != null)
			visit(ctx.varInitValue);
		tabs--;
		
		return null;
	}
	
	public Void visitCase(CoolParser.CaseContext ctx) {
		System.out.println(printTabs() + "case");
		tabs++;
		visit(ctx.cond);
		visit(ctx.caseBranches());
		tabs--;
		
		return null;
	}
	
	public Void visitCaseBranches(CoolParser.CaseBranchesContext ctx) {
		for (var child : ctx.children) {
			visit(child);
		}
		
		return null;
	}
	
	public Void visitCaseBranch(CoolParser.CaseBranchContext ctx) {
		System.out.println(printTabs() + "case branch");
		tabs++;
		visit(ctx.varName);
		visit(ctx.varType);
		visit(ctx.expr());
		tabs--;
		
		return null;
	}
	
	public Void visitBlock(CoolParser.BlockContext ctx) {
		System.out.println(printTabs() + "block");
		tabs++;
		visit(ctx.blockExprs());
		tabs--;
		
		return null;
	}
	
	public Void visitNew(CoolParser.NewContext ctx) {
		System.out.println(printTabs() + "new");
		tabs++;
		visit(ctx.type());
		tabs--;
		
		return null;
	}
	
	public Void visitIsvoid(CoolParser.IsvoidContext ctx) {
		System.out.println(printTabs() + "isvoid");
		tabs++;
		visit(ctx.expr());
		tabs--;
		
		return null;
	}

//	
//	Regulile de visit ale operatiilor aritmetice
//
	
	public Void visitPlus(CoolParser.PlusContext ctx) {
		System.out.println(printTabs() + "+");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitMinus(CoolParser.MinusContext ctx) {
		System.out.println(printTabs() + "-");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitMul(CoolParser.MulContext ctx) {
		System.out.println(printTabs() + "*");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitDiv(CoolParser.DivContext ctx) {
		System.out.println(printTabs() + "/");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitComplement(CoolParser.ComplementContext ctx) {
		System.out.println(printTabs() + "~");
		tabs++;
		visit(ctx.expr());
		tabs--;
		
		return null;
	}

//	
//	Regulile de visit ale operatiilor logice
//
	
	public Void visitLess(CoolParser.LessContext ctx) {
		System.out.println(printTabs() + "<");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitLessEqual(CoolParser.LessEqualContext ctx) {
		System.out.println(printTabs() + "<=");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitEqual(CoolParser.EqualContext ctx) {
		System.out.println(printTabs() + "=");
		tabs++;
		visit(ctx.leftExpr);
		visit(ctx.rightExpr);
		tabs--;
		
		return null;
	}
	
	public Void visitNot(CoolParser.NotContext ctx) {
		System.out.println(printTabs() + "not");
		tabs++;
		visit(ctx.expr());
		tabs--;
		
		return null;
	}

	
	
	
//	
//	Regulile de visit ale terminalilor
//	
	
//	Printam tipul
	public Void visitType(CoolParser.TypeContext ctx) {
		System.out.println(printTabs() + ctx.getText());
		
		return null;
	}
	
	public Void visitId(CoolParser.IdContext ctx) {
		System.out.println(printTabs() + ctx.getText());
		
		return null;
	}
	
	public Void visitInteger(CoolParser.IntegerContext ctx) {
		System.out.println(printTabs() + ctx.getText());
		
		return null;
	}
	
	public Void visitBool(CoolParser.BoolContext ctx) {
		System.out.println(printTabs() + ctx.getText());
		
		return null;
	}
	
	public Void visitString(CoolParser.StringContext ctx) {
		System.out.println(printTabs() + ctx.getText());
		
		return null;
	}
}

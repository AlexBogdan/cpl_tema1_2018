parser grammar CoolParser;

options {
    tokenVocab = CoolLexer;
}

@header{
    package cool.parser;
}

// Definitia unui program
program
    :  	classDeclaration+
    ;

// Definitia unei clase
classDeclaration
	:	CLASS className=type (INHERITS classInherited=type)? classBody SEMICOLON
	;

classBody
	:	OPENED_CURL_BRACKET (feature)* CLOSED_CURL_BRACKET
	;

feature
	:	(attribute | method)
	;

// Definim un atribut
attribute
	:	attrName=id COLON attrType=type (ASSIGNMENT attrInitValue=expr)? SEMICOLON
	;

// Definim o functie
method
	:	methodName=id methodHeader COLON methodType=type methodBody SEMICOLON
	;

methodHeader
	:	OPENED_ROUND_BRACKET ((formal COMMA)* formal)? CLOSED_ROUND_BRACKET
	;

methodBody
	:	OPENED_CURL_BRACKET expr CLOSED_CURL_BRACKET
	;

type
	:	SELFTYPE | CLASSNAME
	;




io
	:	CACAT
	;




formal
	:	formalName=id COLON formalType=type
	;



expr
	:	expr cast? POINT functionCall					#dispatch
	|	functionCall									#implicitDispatch
	|	IF cond=expr THEN body1=expr ELSE body2=expr FI	#if
	|	WHILE cond=expr LOOP body=expr POOL				#while
	|	LET vars IN body=expr							#let
	|	CASE cond=expr OF caseBranches	ESAC					#case
	|	OPENED_CURL_BRACKET body=blockExprs CLOSED_CURL_BRACKET	#block
	|	NEW type										#new
	|	ISVOID expr										#isvoid
	|	leftExpr=expr MUL rightExpr=expr				#mul
	|	leftExpr=expr DIV rightExpr=expr				#div
	|	leftExpr=expr PLUS rightExpr=expr				#plus
	|	leftExpr=expr MINUS rightExpr=expr				#minus
	|	COMPLEMENT expr									#complement
	|	leftExpr=expr LESS rightExpr=expr				#less
	|	leftExpr=expr LESSEQUAL rightExpr=expr			#lessEqual
	|	leftExpr=expr EQUAL rightExpr=expr				#equal
	|	NOT expr										#not
	|	leftExpr=id ASSIGNMENT rightExpr=expr			#assignment
	|	OPENED_ROUND_BRACKET expr CLOSED_ROUND_BRACKET 	#bracket
	|	id												#id_name	
	| 	INTEGER											#integer
	| 	STRING											#string
	| 	TRUE											#bool
	|	FALSE											#bool
	;

cast
	:	AROND type
	;

functionCall
	:	functionName=id OPENED_ROUND_BRACKET ((expr COMMA)* expr)? CLOSED_ROUND_BRACKET
	;

vars
	:	(var=local COMMA)* var=local
	;

local
	:	varName=id COLON varType=type (ASSIGNMENT varInitValue=expr)?
	;

caseBranches
	:	caseBranch+
	;

caseBranch
	:	varName=id COLON varType=type IMPLIES expr SEMICOLON
	;
	
blockExprs
	:	(expr SEMICOLON)*
	;

id
	:	ID
	;
